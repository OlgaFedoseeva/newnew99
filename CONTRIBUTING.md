Отзывы, пожелания и предложения можно отправлять по электронной почте.<br>
Конструктивная критика, описания ошибок и прочие отчёты приветствуются.<br>
Адрес электронной почты: gennadiy.golovin@internet.ru

---

Feedback, wishes and suggestions can be sent over electronic mail.<br>
Constructive criticism, error descriptions and other reports are welcome.<br>
Electronic mail address: gennadiy.golovin@internet.ru
