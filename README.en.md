## [Site pages](README.md)

- [Winograd — Strassen algorithm](https://pomodoro3.mircloud.ru/en/2022/02/11/winograd-strassen-algorithm.html) — 11.02.2022.
- [Matrix multiplication in parallel streams](https://pomodoro3.mircloud.ru/en/2022/02/09/matrix-multiplication-parallel-streams.html) — 09.02.2022.
- [Matrix rotation 180 degrees](https://pomodoro3.mircloud.ru/en/2021/12/17/matrix-rotation-180-degrees.html) — 17.12.2021.
- [Matrix rotation 90 degrees](https://pomodoro3.mircloud.ru/en/2021/12/13/matrix-rotation-90-degrees.html) — 13.12.2021.
- [Optimizing matrix multiplication](https://pomodoro3.mircloud.ru/en/2021/12/10/optimizing-matrix-multiplication.html) — 10.12.2021.

### Source texts

- Series of the static web-sites [«Pomodori»](https://hub.mos.ru/golovin.gg/about/blob/master/README.en.md).
- Used formats — Markdown, Liquid, YAML.
- Build tool — Jekyll with tomato design themes.
- Automation of processes — Shell scripts.

### Scripts

1. [build.sh](build.sh) — Building a site in two tomato themes and optimizing the results.
2. [serve.sh](serve.sh) — Local deployment to check whether the build is correct.
3. [repository.sh](repository.sh) — Preparing a repository for subsequent publication.
4. [package.sh](package.sh) — Preparing a package for subsequent deployment.
