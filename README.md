## [Страницы сайта](README.en.md)

- [Алгоритм Винограда — Штрассена](https://pomodoro3.mircloud.ru/ru/2022/02/10/winograd-strassen-algorithm.html) — 10.02.2022.
- [Умножение матриц в параллельных потоках](https://pomodoro3.mircloud.ru/ru/2022/02/08/matrix-multiplication-parallel-streams.html) — 08.02.2022.
- [Поворот матрицы на 180 градусов](https://pomodoro3.mircloud.ru/ru/2021/12/16/matrix-rotation-180-degrees.html) — 16.12.2021.
- [Поворот матрицы на 90 градусов](https://pomodoro3.mircloud.ru/ru/2021/12/12/matrix-rotation-90-degrees.html) — 12.12.2021.
- [Оптимизация умножения матриц](https://pomodoro3.mircloud.ru/ru/2021/12/09/optimizing-matrix-multiplication.html) — 09.12.2021.

### Исходные тексты

- Серия статических вёб-сайтов [«Помидоры»](https://hub.mos.ru/golovin.gg/about/blob/master/README.md).
- Используемые форматы — Markdown, Liquid, YAML.
- Инструмент сборки — Jekyll с помидорными темами оформления.
- Автоматизация процессов — Shell сценарии командной строки.

### Сценарии

1. [build.sh](build.sh) — Сборка сайта в двух помидорных темах и оптимизация результатов.
2. [serve.sh](serve.sh) — Локальное развёртывание для проверки корректности сборки.
3. [repository.sh](repository.sh) — Подготовка репозитория для последующей публикации.
4. [package.sh](package.sh) — Подготовка пакета для последующего развёртывания.
